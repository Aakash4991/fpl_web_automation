package com.bean;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class LoginBean extends BaseDataBean {

	private String userName;
	private String password;
	
	public String getUserName() {
		return userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	}

