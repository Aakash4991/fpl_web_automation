package com.fplautomation;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.utilities.utils;

public class common extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}
	utils utils = new utils();
	@QAFTestStep(description = "user on landing page")
	public void userOnLandingPage() {
		driver.manage().deleteAllCookies();
		driver.get("/");
		driver.manage().window().maximize();
	}

	@QAFTestStep(description = "user should see home page")
	public void verifyHomePage() {

		if (verifyPresent("logout.button.loc")) {
			Validator.verifyThat("logout button is present",
					verifyPresent("logout.button.loc"), Matchers.equalTo((true)));
		} else if (verifyPresent("popup.close.loc")) {
			click("popup.close.loc");
			Validator.verifyThat("logout button is present",
					verifyPresent("logout.button.loc"), Matchers.equalTo((true)));

		}
	}

	@QAFTestStep(description = "user click on {0}")
	public void clickOnbutton(String btn) {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		QAFExtendedWebElement element = new QAFExtendedWebElement(String
				.format(ConfigurationManager.getBundle().getString("all.link.loc"), btn));
		element.waitForVisible(3000);

		element.click();
	}

	@QAFTestStep(description = "user click on {0} link")
	public void clickOnLink(String link) {
		waitForPageToLoad();
		QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("header.link.loc"), link));
		element.waitForVisible(3000);

		element.click();
	}
	
	@QAFTestStep(description = "user click {0} Button")
	public void clickButtons(String btn) {

		try {
			QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
					ConfigurationManager.getBundle().getString("all.button.loc"), btn));
			element.waitForVisible(4000);
			if (element.verifyPresent()) {

				Reporter.log("Button is present");
				element.click();
			}

		} catch (Exception e) {
			Reporter.log("Button is not present");

		}
	}

	@QAFTestStep(description = "user click {0} link buttons")
	public void clickOnLinkButtons(String link) {
		waitForPageToLoad();
		QAFExtendedWebElement element = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("link.button.loc"), link));
		element.waitForVisible(3000);

		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", element);

	}
	
	
	public void clickUsingJavaScript(String loc) {
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].click();", new QAFExtendedWebElement(loc));
	}

	public static List<QAFWebElement> getQAFWebElements(String loc) {
		return new QAFExtendedWebElement(
				ConfigurationManager.getBundle().getString("html.common"))
						.findElements(loc);
	}
	
	
	public static boolean verifyMessageVisible(String loc, String message) {
		boolean result = false;
		List<QAFWebElement> elements = common.getQAFWebElements(loc);
		for (QAFWebElement ele : elements) {
			if (ele.getText().trim().toLowerCase().contains(message.toLowerCase())) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	public String removeSpace(String text) {
		text= text.replaceAll(" ", "");
		System.out.println("remove space"+text);
		return text;
	}
	
	public static void scrollUpToElement(QAFWebElement ele) {
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView()", ele);
	}
}
