package com.fplautomation;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.bean.AddressBean;
import com.bean.LoginBean;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;

import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class login extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}
	common common = new common();

	@QAFTestStep(description = "user enter login details as {0}")
	public void loginDetails(String key) {
		QAFTestBase.pause(5000);
		LoginBean loginBean = new LoginBean();
		loginBean.fillFromConfig(key);
		click("email.login.loc");
		Reporter.log(loginBean.getUserName());
		Reporter.log(loginBean.getPassword());
		sendKeys(loginBean.getUserName(), "email.login.loc");
		click("password.login.loc");

		sendKeys(loginBean.getPassword(), "password.login.loc");
		click("login.button.loc");

	}

	@QAFTestStep(description = "user click on my account link")
	public void clickOnMyAccountLink() {
		waitForPageToLoad();
		click("myaccount.link.loc");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@QAFTestStep(description = "user click on pay bill button")
	public void clickOnLinkButton() {
		verifyPresent("pay.bill.loc");
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("window.scrollBy(0,600)", "");
		common.clickUsingJavaScript("pay.bill.loc");
	
	
}
}
