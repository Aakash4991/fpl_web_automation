package com.utilities;

import java.util.List;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class utils {

	
	public static List<QAFWebElement> getQAFWebElements(String loc) {
		return new QAFExtendedWebElement(
				ConfigurationManager.getBundle().getString("html.common"))
						.findElements(loc);
	}
}
