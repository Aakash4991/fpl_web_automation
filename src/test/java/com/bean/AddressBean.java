package com.bean;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class AddressBean extends BaseDataBean {

	private String phoneNumber;
	private String zipcode;
	
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public String getZipcode() {
		return zipcode;
	
	
	}
	
}
