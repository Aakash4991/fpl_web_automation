package com.fplautomation;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;

import com.bean.AddressBean;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class poweroutage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}
	common common = new common();

	@QAFTestStep(description = "fill required power outage details as {0}")
	public void powerOutageDetails(String key) {
		QAFTestBase.pause(5000);
		AddressBean addressBean = new AddressBean();
		addressBean.fillFromConfig(key);
		sendKeys(addressBean.getPhoneNumber(), "primary.phone.loc");

		sendKeys(addressBean.getZipcode(), "zip.code.loc");

		String s = common.removeSpace(new QAFExtendedWebElement("capcha.text.loc").getText());
		sendKeys(s, "capcha.textbox.loc");

	}

	@QAFTestStep(description = "user click on edit profile")
	public void clickOnEditProfile() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		CommonStep.waitForEnabled("edit.profile.loc");
		click("edit.profile.loc");
	}

}
